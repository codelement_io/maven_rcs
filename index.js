const sites = require('./resources/sites')
const siteConfiguration = require('./builders/SiteConfiguration').siteConfiguration;

for(let site of  sites){

    siteConfiguration.configure(site)
                     .grantLPAjspAdminPermissions()
                     .logIntoLiveEngage()
                     .prepareSmartConnectorWidget()
                     .installSmartConnectorWidget()
                     .createMavenSkill()
                     .getSiteSkillRules()
                     .updateSiteSkillRules()
                     .createLiveEngageSmartConnectorWidget()
                     .createLiveEngageSmartConnectorApi()
                     .setSmartConnectorBotUserProfiles()
                     .enableACFeatures()
                     .createSmartConnectorUser()
                     .saveConfiguration()
}

// const configureNextSite = function(siteIndex) {(siteIndex < sites.length) ? configureSite(sites[siteIndex], siteIndex) : "";}

// configureSite(sites[0], 0)
