
const RequestHelper = {

    getAuthHeaderBySite: (token, userId, account,revisionNumber) => {
        return {
                "Authorization": "Bearer "+token,
                "X-LP-ACCOUNT": account,
                "X-LP-TOKEN": token,
                "X-LP-USER": userId,
                "If-Match": revisionNumber
              };
    },


    getConnectorConfigBySite: (account,subDomain) => {

        return {
                "client_name": "SmartConnect Agent Widget",
                "grant_types": [
                    "authorization_code",
                    "refresh_token"
                ],
                "redirect_uris": [
                "https://"+subDomain+".ivrdeflect.liveperson.net/"+account+"/callback?view=ivragentview"
                ]
            };
    },

    getWidgetConfigBySite: (skill, subDomain) => {

        return  {
                    "description": "SmartConnect Widget",
                    "enabled": true,
                    "mode": "published",
                    "name": "SmartConnect Widget",
                    "parameters": [],
                    "skillIds": [skill],
                    "span": "2",
                    "type": "iFrame",
                    "url": "https://"+subDomain+".ivrdeflect.liveperson.net/login.html",
        }       
    },

    getSiteConfiguration: (account, client_id, client_secret,botUserName,appKey,secret,accessToken,accessTokenSecret) => {

        return {
                    "siteId": account,
                    "agentloginClientId": client_id,
                    "agentloginClientSecret": client_secret,
                    "botUserName": botUserName,
                    "appKey": appKey,
                    "secret": secret,
                    "accessToken": accessToken,
                    "accessTokenSecret": accessTokenSecret
                };
    },

    getSingleSkillRuleRequest: (skill, orderId, serviceId) =>{

        return {
            "orderId": orderId,
            "description": "Vodafone RCS",
            "conditions": [{
                "type": "sde",
                "data": {
                    "include": [{
                        "type": "text",
                        "data": {
                            "type": "equals",
                            "value": serviceId
                        }
                    }]
                },
                "sdeType": "ctmrinfo",
                "field": "info.companyBranch"
            }],
            "outcomes": [{
                "type": "skill",
                "skillId": skill
            }]
        }
    },

    getFullSkillRuleRequest: (skill, orderId, serviceId) => {

        return {
                "id": "le.site.skill.selection.definition",
                "propertyValue": {
                    "value": {
                        "rules": [{
                            "orderId": orderId,
                            "description": "Vodafone RCS",
                            "conditions": [{
                                "type": "sde",
                                "data": {
                                    "include": [{
                                        "type": "text",
                                        "data": {
                                            "type": "equals",
                                            "value": serviceId
                                        }
                                    }]
                                },
                                "sdeType": "ctmrinfo",
                                "field": "info.companyBranch"
                            }],
                            "outcomes": [{
                                "type": "skill",
                                "skillId": skill
                            }]
                        }]
                    }
                },
                "type": "4"
        }
    },

    getSkillRequest: () => {

        return {
                "id": null,
                "name": "maven",
                "description": "Vodafone RCS",
                "maxWaitTime": "120",
                "skillRoutingConfiguration": [],
                "defaultPostChatSurveyId": null,
                "defaultOfflineSurveyId": null,
                "defaultAgentSurveyId": null,
                "wrapUpTime": null,
                "lobIds": [],
                "canTransfer": true,
                "skillTransferList": [],
                "slaDefaultResponseTime": null,
                "slaUrgentResponseTime": null,
                "slaFirstTimeResponseTime": null,
                "transferToAgentMaxWaitInSeconds": null,
                "workingHoursId": null,
                "specialOccasionId": null,
                "autoCloseInSeconds": null,
                "fallbackSkill": null,
                "fallbackWhenAllAgentsAreAway": true
        }
    },

    getLEApiRequest: () => {

        return {
            "developerID": "George Tsang",
            "appName": "SmartConnect Bot",
            "purpose": "LIVE",
            "privileges": [{
                "type": "API",
                "data": "api.user.login"
            }, {
                "type": "API",
                "data": "interaction.history.api.external"
            }, {
                "type": "API",
                "data": "ac.users.users.read.external"
            },{
                "type": "API",
                "data": "ac.users.skills.read.external"
            }],
            "enabled": true,
            "appDescription": "SmartConnect Bot",
            "ipRanges": []
        }
    },

    getLEUserRequest: (apiKeyId,skillId, agentRoleId, managerRoleId) => {

        return {
                "id": null,
                "pid": null,
                "deleted": false,
                "loginName": "mavenbot",
                "userTypeId": 2,
                "isApiUser": true,
                "allowedAppKeys": apiKeyId,
                "email": "gtsang@liveperson.com",
                "fullName": "Maven Bot",
                "employeeId": null,
                "nickname": "Maven Bot",
                "isEnabled": true,
                "maxChats": 4,
                "maxAsyncChats": null,
                "isActive": true,
                "skillIds": [skillId],
                "memberOf": {
                    "agentGroupId": -1
                },
                "managerOf": [{
                    "agentGroupId": -1
                }],
                "pictureUrl": null,
                "profileIds": [agentRoleId, managerRoleId],
                "changePwdNextLogin": false,
                "lobIds": [],
                "isMyUser": false
                }
    },

    getBotTypeACFeature: (account) => {

        return {
                "appDataList": [{
                    "appName": "provision",
                    "appApiVersion": 2,
                    "accountList": {
                        "accountList": [{
                            "siteId": account,
                            "itemsCollection": {
                                "data": [{
                                    "compoundFeatureID": "Common.User_type_bot",
                                    "startDate": "1969-12-31 00:00:00",
                                    "endDate": "2999-12-31 00:00:00",
                                    "value": {
                                        "value": true
                                    },
                                    "isDeleted": false
                                }, {
                                    "compoundFeatureID": "Common.API_User_Login",
                                    "startDate": "1969-12-31 00:00:00",
                                    "endDate": "2999-12-31 00:00:00",
                                    "value": {
                                        "value": true
                                    },
                                    "isDeleted": false
                                }]
                            }
                        }]
                    }
                }]
            }
    },


}

module.exports = {
    RequestHelper
}