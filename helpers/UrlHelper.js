'use strict';
require('dotenv').config()
const UrlHelper = {

    getDomainUrl: (account, service) => {
        return "http://api.liveperson.net/api/account/"+account+"/service/"+service+"/baseURI.json?version=1.0";   
    },

    getLoginUrl: (domain, account) => {
       return  "https://"+domain+"/api/account/"+account+"/login?v=1.3";
    },

    getHoustonNewAppManagementUrl: (account,domain) => {
        return "https://"+domain+"/api/account/"+account+"/configuration/app-install/installations?v=1.0";
    },

    getHoustonAppManagementInstallationsUrl: (account, domain) => {      
        return "https://"+domain+"/api/account/"+account+"/configuration/app-install/installations?v=1.0";
    },

    getLECreateWidgetUrl: (account,domain) =>{
        return "https://"+domain+"/api/account/"+account+"/configuration/le-ui-personalization/widgets?v=2.0&__d=46692";
    },
    
    getLESkillsUrl: (domain, account) => {
        return "https://"+domain+"/api/account/"+account+"/configuration/le-users/skills";
    },

    getSkillRuleUrl: (account, domain) => {
        return "https://"+domain+"/api/account/"+account+"/configuration/setting/properties/le.site.skill.selection.definition?v=3.0";
    },

    getJspAdminUrl: (account, domain) => {

        return "https://"+domain+"/hc/web/public/pub/siteadminlogin.jsp?goto=siteadmin/LPAdminAccess.jsp&user="+process.env.LPA_USER_NAME+"&pass="+process.env.ENCODED_PASSWORD+"&site="+account;
    },

    getJspAdminReasonUrl: (account, domain) =>{

        return "https://"+domain+"/hc/web/siteadmin/siteadmin/LPAdminAccess.jsp?site="+account
    },

    getAppKeyManagementUrl: (account, domain) => {

        return " https://"+domain+"/app-key-manager/api/account/"+account+"/keys?v=1.0&__d=66780";
    },

    getLEProfilesUrl: (account, domain) => {

        return "https://"+domain+"/api/account/"+account+"/configuration/le-users/profiles";
    },

    getLECreateUserUrl: (account, domain) => {

        return "https://"+domain+"/api/account/"+account+"/configuration/le-users/users";
    },

    getLEBotTypeAcFeatureURl: (account, domain) => {
        return "https://"+domain+"/api/account/"+account+"/configuration/provision/featureGrants?v=2.0&excludeLegacy=true&jsonProvider=gson"
    },

    getAllSkillRulesUrl: (account, domain) => {
        return "https://"+domain+"/api/account/"+account+"/configuration/setting/properties/le.site.skill.selection.definition?v=3.0";
    },

    getLPProfileUrl: (domain, account,profileId) => {
        return "https://"+domain+"/api/account/"+account+"/configuration/le-users/profiles/"+profileId+"?v=4.0&select=$all&_=1564967378336&__d=91648"
    },

    getAllSkillsUrl: (domain, account) => {
        return "https://"+domain+"/api/account/"+account+"/configuration/le-users/skills?v=2.0&select=$all&_=1565458158875&__d=60412"
   
    },
    getAllLEWidgetsUrl: (domain, account) => {
        return "https://"+domain+"/api/account/"+account+"/configuration/le-ui-personalization/widgets?v=2.0&select=$all&return=active&_=1565461690344&__d=39659"
    },

    getAllUsersUrl: (domain, account) => {
        return "https://"+domain+"/api/account/"+account+"/configuration/le-users/users?v=4.0&select=id,pid,deleted,userTypeId,isApiUser,email,loginName,nickname,fullName,employeeId,isEnabled,resetMfaSecret,maxChats,maxAsyncChats,skillIds,memberOf(agentGroupId,assignmentDate),managerOf(agentGroupId,assignmentDate),profileIds,lobIds&userTypeId=[1,2]&_=1565579435590&__d=6462"
    }

}

module.exports = {
    UrlHelper
}